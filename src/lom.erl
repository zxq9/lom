%%% @doc
%%% List of Maps: lom
%%%
%%% This module provides equivalents to the lists:key*/N functions in the
%%% stdlib lists module: http://erlang.org/doc/man/lists.html#keydelete-3
%%% While the stdlib functions operate over lists of records, these functions
%%% operate over lists of maps.
%%%
%%% In the common case lists of commonly shaped data generated within a system
%%% tends to be lists of records, but in the case of external data imported to
%%% a system using a parser for syntaxes like JSON, ASN.1, YAML, Python dicts,
%%% XML, etc. the data will be converted to lists of maps, and this module is
%%% intended to make those cases just as convenient to deal with as the native
%%% case of lists of records.
%%% @end

-module(lom).
-vsn("1.0.0").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([new/0,
         store/3,
         take/3,
         replace/3,
         delete/3,
         find/3,
         member/3,
         index/3,
         index/4,
         keymap/3,
         sort/2,
         merge/3]).

-export_type([map_list/0]).


-type map_list() :: [map()].


-spec new() -> map_list().
%% @doc
%% Create a new, empty list of maps.

new() ->
    [].


-spec store(Key, MapList, NewMap) -> NewMapList
    when Key        :: term(),
         MapList    :: map_list(),
         NewMap     :: map(),
         NewMapList :: map_list().
%% @doc
%% Stores a map using the value `Key' as its index within the list of maps,
%% replacing any previously existing entry with the same value.

store(Key, MapList, NewMap) ->
    #{Key := Value} = NewMap,
    store(Key, Value, MapList, NewMap).

store(K, V, [H | T], M) ->
    case maps:get(K, H) =:= V of
        false -> [H | store(K, V, T, M)];
        true  -> [M | T]
    end;
store(_, _, [], M) ->
    [M].


-spec take(Key, Value, MapList) -> Result
    when Key        :: term(),
         Value      :: term(),
         MapList    :: map_list(),
         Result     :: {value, Found, NewMapList}
                     | false,
         Found      :: map(),
         NewMapList :: map_list().
%% @doc
%% Finds the first map in the list where `#{Key := Value} = Member' is true and
%% returns it along with a copy of the list minus that map. If no map is found
%% returns the atom `false'.

take(Key, Value, MapList) ->
    take(Key, Value, MapList, []).

take(K, V, [H | T], A) ->
    case maps:get(K, H) =:= V of
        false -> take(K, V, T, [H | A]);
        true  -> {value, H, lists:reverse(A, T)}
    end;
take(_, _, [], _) ->
    false.


-spec replace(Key, MapList, NewMap) -> NewMapList
    when Key        :: term(),
         MapList    :: map_list(),
         NewMap     :: map(),
         NewMapList :: map_list().
%% @doc
%% Replaces the first occurrence of a map where the value of `Key' matches that of the
%% provided `NewMap', if such a map exists in the list.

replace(Key, MapList, NewMap) ->
    #{Key := Value} = NewMap,
    replace(Key, Value, MapList, NewMap).

replace(K, V, [H | T], M) ->
    case maps:get(K, H) =:= V of
        false -> [H | replace(K, V, T, M)];
        true  -> [M | T]
    end;
replace(_, _, [], _) ->
    [].


-spec delete(Key, Value, MapList) -> NewMapList
    when Key        :: term(),
         Value      :: term(),
         MapList    :: map_list(),
         NewMapList :: map_list().
%% @doc
%% Removes the first instance of a map where the `Value' of `Key' matches
%% the one provided.

delete(Key, Value, [H | T]) ->
    case maps:get(Key, H) =:= Value of
        false -> [H | delete(Key, Value, T)];
        true  -> T
    end;
delete(_, _, []) ->
    [].


-spec find(Key, Value, MapList) -> Result
    when Key     :: term(),
         Value   :: term(),
         MapList :: map_list(),
         Result  :: map() | false.
%% @doc
%% Returns the first map in the list where the `Value' of `Key' matches the
%% one provided, or the atom `false' if such a map does not exist.

find(Key, Value, [H | T]) ->
    case maps:get(Key, H) =:= Value of
        false -> find(Key, Value, T);
        true  -> H
    end;
find(_, _, []) ->
    false.


-spec member(Key, Value, MapList) -> boolean()
    when Key     :: term(),
         Value   :: term(),
         MapList :: map_list().
%% @doc
%% Returns `true' if a member of the list has a `Key' that matches `Value', and
%% `false' otherwise.

member(Key, Value, [H | T]) ->
    case maps:get(Key, H) =:= Value of
        false -> member(Key, Value, T);
        true  -> true
    end;
member(_, _, []) ->
    false.


-spec index(Key, Value, MapList) -> Result
    when Key     :: term(),
         Value   :: term(),
         MapList :: map_list(),
         Result  :: pos_integer() | error.
%% @doc
%% Return the 1-based index of the first map in the list that matches the provided
%% `Key' and `Value'.

index(Key, Value, MapList) ->
    index(Key, Value, MapList, 1).


-spec index(Key, Value, MapList, StartIndex) -> Result
    when Key        :: term(),
         Value      :: term(),
         MapList    :: map_list(),
         StartIndex :: integer(),
         Result     :: integer() | error.
%% @doc
%% Return the index of the first map in the list that matches the provided
%% `Key' and `Value', starting at whatever value is provided for `StartIndex'.

index(Key, Value, [H | T], Index) ->
    case maps:get(Key, H) =:= Value of
        false -> index(Key, Value, T, Index + 1);
        true  -> Index
    end;
index(_, _, [], _) ->
    error.


-spec keymap(Fun, Key, MapList) -> NewMapList
    when Fun        :: fun(),
         Key        :: term(),
         MapList    :: map_list(),
         NewMapList :: map_list().
%% @doc
%% Maps the function `Fun' onto the value at `Key' for every map of the list.

keymap(Fun, Key, [H | T]) ->
    [maps:update_with(Key, Fun, H) | keymap(Fun, Key, T)];
keymap(_, _, []) ->
    [].


-spec sort(Key, MapList) -> Sorted
    when Key     :: term(),
         MapList :: map_list(),
         Sorted  :: map_list().
%% @doc
%% Sorts a list of maps on the values at `Key'.

sort(Key, MapList) ->
    Ordering =
        fun(A, B) ->
            VA = maps:get(Key, A),
            VB = maps:get(Key, B),
            VA =< VB
        end,
    lists:sort(Ordering, MapList).


-spec merge(Key, MapList1, MapList2) -> Merged
    when Key      :: term(),
         MapList1 :: map_list(),
         MapList2 :: map_list(),
         Merged   :: map_list().
%% @doc
%% Merges two lists sorted on `Key', maintaining sort order.
%% If the value of `Key' compares the same for two elements, then the element
%% from `MapList1' is picked.

merge(Key, L1 = [H1 | T1], L2 = [H2 | T2]) ->
    V1 = maps:get(Key, H1),
    V2 = maps:get(Key, H2),
    if
        V1  <  V2 -> [H1 | merge(Key, T1, L2)];
        V1  >  V2 -> [H2 | merge(Key, L1, T2)];
        V1 =:= V2 -> [H1 | merge(Key, T1, T2)]
    end;
merge(_, L1, []) ->
    L1;
merge(_, [], L2) ->
    L2.
