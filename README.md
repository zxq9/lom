# Lists of Maps

Manipulation functions for lists of maps (as opposed to lists of records).
These follow the same conventions and argument order found in the stdlib 'lists:key\*/N' functions.

HTML docs can be found here: [http://zxq9.com/projects/lom/docs/](http://zxq9.com/projects/lom/docs/)